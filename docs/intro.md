---
sidebar_position: 1
---
# Getting Started

With the Rainbow Design System as its foundation, the system consists of working code, design tools and resources. The component libraries give developers a collection of reusable components for building websites and user interfaces.