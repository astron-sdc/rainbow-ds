---
sidebar_position: 1
---

# Typography

## Overview
This will be an **introduction text** and **overview** of a UI component. We can also link to a blog or release note here.
- Version 1.0.0 - 10 October 2023 - Typography added to documentation and DS-package.

## Interface
Description about the component **when (not) to use**, **behaviors**, **states** and **variants**. For all Astron applications we use 2 typfaces (Montserrat and Inter) that are designed for screens and screen-readibility.

### Usage

#### Use typography
Adding structure to application with heading and body texts.
- For user interface headings we use typface: `{font-family: 'Montserrat, Arial, sans-serif;}`
- For user interface body text we use typface: `{font-family: 'Inter', Arial, sans-serif;}`

#### Do not use typography
When using image(s) and or video(s) as content.


## Code

Explain formatting and related modifiers etc.
Copy code `<h1>This is a header</h1>`

### Demo
Maybe include a live demo mode.