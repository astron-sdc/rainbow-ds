---
sidebar_position: 4
---

# Buttons
 
## Overview
This will be an **introduction text** and **overview** of a UI component. We can also link to a blog or release note here.
- Version 1.0.0 - 10 October 2023 - Typography added to documentation and DS-package.

## Interface
Description about the component **when (not) to use**, **behaviors**, **states** and **variants**. 

### Usage
#### Use buttons
#### Do not use buttons

### Behaviors
### States
### Variants
## Code
### Demo
-