---
sidebar_position: 2
---

# Colors

## Overview
This will be an **introduction text** and **overview** of a UI component. We can also link to a blog or release note here.
- Version 1.0.0 - 10 October 2023 - Colors added to documentation and DS-package.

## Interface
For all Astron applications we use 2 branding colors (blue variants) and additional rainbow UI colors.

### Usage

#### Use colors
Adding structure to application with heading and body texts.

##### Astron Branding
- <img valign='middle' alt='Light Blue' src='https://readme-swatches.vercel.app/00ADEE'/> #00ADEE - 'Astron Light Blue'
- <img valign='middle' alt='Blue' src='https://readme-swatches.vercel.app/3A5896'/> #3A5896 - 'Astron Blue'

##### Astron Additional Rainbow
- <img valign='middle' alt='Yellow' src='https://readme-swatches.vercel.app/EEEE22'/> #EEEE22 - 'Astron Yellow'
- <img valign='middle' alt='Red' src='https://readme-swatches.vercel.app/FF4444'/> #FF4444 - 'Astron Red'
- <img valign='middle' alt='Orange' src='https://readme-swatches.vercel.app/FF9933'/> #FF9933 - 'Astron Orange'
- <img valign='middle' alt='Green' src='https://readme-swatches.vercel.app/22CC22'/> #22CC22 - 'Astron Green'
- <img valign='middle' alt='Violet' src='https://readme-swatches.vercel.app/DD55FF'/> #DD55FF - 'Astron Violet'
- <img valign='middle' alt='Turquoise' src='https://readme-swatches.vercel.app/11BBAA'/> #11BBAA - 'Astron Turquoise'

##### Astron Base UI
- <img valign='middle' id='bordered' alt='White' src='https://readme-swatches.vercel.app/FFFFFF'/> #FFFFFF - 'Base White'
- <img valign='middle' alt='Black' src='https://readme-swatches.vercel.app/000000'/> #000000 - 'Base Black'
- <img valign='middle' alt='Light Gray' src='https://readme-swatches.vercel.app/DADADA'/> #DADADA - 'Light Gray'
- <img valign='middle' alt='Gray' src='https://readme-swatches.vercel.app/777777'/> #777777 - 'Gray'
- <img valign='middle' alt='Medium Gray' src='https://readme-swatches.vercel.app/313131'/> #313131 - 'Medium Gray'
- <img valign='middle' alt='Dark Mode Blue' src='https://readme-swatches.vercel.app/181828'/> #181828 - 'Dark Mode Blue'

#### Do not use colors
When designs have conflicting with contrast and affect bad readibility. You can always use our color guidelines.

### Behaviors


### States
### Variants

## Code

Explain formatting and related modifiers etc.
Copy code `<h1>This is a header</h1>`

### Demo
Maybe include a live demo mode.