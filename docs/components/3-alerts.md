---
sidebar_position: 3
---

# Alerts

## Overview
This will be an **introduction text** and **overview** of a UI component. We can also link to a blog or release note here.
- Version 1.0.0 - 10 October 2023 - Typography added to documentation and DS-package.

## Interface
Description about the component **when (not) to use**, **behaviors**, **states** and **variants**. 

### Usage
#### Use alerts
#### Do not use alerts

### Behaviors
### States
### Variants
## Code
### Demo
