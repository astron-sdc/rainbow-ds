# Templates
(Work in progress)
Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam et vestibulum mauris. Cras vitae hendrerit diam.

## Landing templates
Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam et vestibulum mauris. Cras vitae hendrerit diam.

### Empty state
Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam et vestibulum mauris. Cras vitae hendrerit diam.
- (Maintain) Design System.

### Simple text
Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam et vestibulum mauris. Cras vitae hendrerit diam.
- Testing (Usability, A/B, Paper Prototyping).

### Dashboard
Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam et vestibulum mauris. Cras vitae hendrerit diam.
- User Interviews.
- Card Sorting.
