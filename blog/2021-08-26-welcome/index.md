---
slug: rainbow
title: (UX) Design Process and Principles
authors: [kevin]
tags: [rainbow,designsystem, hello, introduction]
---

Hi! I’m Kevin Tai living in Assen. I will be your new UX/UI Designer at Astron. I'm assigned to make Astron’s first Design System and make better products and improve user experience. For the foundation of the Design System and UX strategy; I decided to follow a Design Process and several Design Principles to output structured and consistent User Interfaces. 

Let's develop better `Products` and `User Experiences` together with the same mission and vision through our foundation! Read about our design `process` and `principles` at the `foundation` section.