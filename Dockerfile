FROM node:lts AS BUILDER

WORKDIR /app
COPY package*.json ./
RUN npm ci
COPY . .
RUN npm run build

FROM nginx:mainline-alpine
COPY --from=BUILDER /app/build /usr/share/nginx/html/rainbow-ds
COPY conf.d/default.conf /etc/nginx/conf.d/default.conf
EXPOSE 80
